
# SMultitaper.jl

This package (small+multitaper) is a very small and very terse implementation of
an adaptively-weighted multitaper estimator. It uses cached FFTs and FFT
work vectors, cached tapers, and cached eigenvalues, so that function calls can
still be very convenient but repeated calls will automatically be very
efficient. I personally like this code because I think it also to some degree
serves as a showcase of how powerful the Julia language is, as it really manages
to do a lot in about 200 lines of code: manual memoization, in-place FFTs,
very flexible keyword arg processing, and more.

As of now, the keyword arg options are: `pad` (for padding an estimator),
`shift` (for applying an fftshift or not), `center` for removing the mean of the
data vector(s), `aweight` for adaptive weighting, `nw` for NW dpss multitaper
parameter, `k` for number of tapers, `tapers` for taper type (`:dpss`, `:sine`,
or a manually provided matrix whose columns are tapers), `eigvals`, for the
option of manually providing eigenvalues for adaptive weighting, and
`aweight_maxit` and `aweight_tol` for tuning the adaptive weighting. See the
source code for default values.

For data vectors `v1` and `v2`, an example call for a multitaper estimator of
the cross-spectrum might look like
```{julia}
spec = multitaper(v1, v2, center=true, aweight=true, nw=5, k=8, shift=true)
```
for using 8 dpss tapers. At the extreme, `multitaper(v1)` gives a DPSS
multitaper estimator for the spectral density of `v1` using default parameters.
But since many things are cached, even this function call is efficient.

There is also a `spectrogram` function, which has the same syntax as
`multitaper` except with the necessary keyword `blocklen` and optional extra
keyword `stepsize`, so that you can do overlapped chunk estimates. It just does
a little pre-allocation and then calls the internal in-place `multitaper!`
function on each data chunk, so it isn't particularly optimal. But it's fast
enough, and it allows adaptive weighting and multiple tapers and other nice
things that don't seem to be offered by other packages.

To change the number of cached tapers, eigenvalue pairs, and FFT
plans/workspaces, changed the module's internal variables with, for example,
`SMultitaper.SETTINGS.TAPERS_STORE_MAX = 10`. 

# Installation

This package isn't registered, but there are still a few options for easy usage.
Considering its simplicity and hackability, my advice would probably be to
simply clone the repo and then add it to your project with
```
julia> ]dev /path/to/cloned/repo/SMultitaper.jl
```
That way you can play with the code and whenever you reload that `Project.toml`
file you'll have the new version of the module. Probably not the most advisable
solution in general and you could of course also do
```
julia> ]add https://bitbucket.org/cgeoga/SMultitaper.jl#master
```
or something. 

