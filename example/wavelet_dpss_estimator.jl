
# Here's an example of computing a further processed SDF estimator, inspired by
# Percival and Walden (Wavelets) pp444-445. This does NOT do anything careful,
# though, and just uses the stock wavelet denoising function in Wavelets.jl on
# the log-transformed multitaper estimator. Probably not way far off, but
# definitely less precise than the method mentioned in PW. Even this very cheap
# thing, though, makes a nice picture.

using FFTW, DSP, SMultitaper, Wavelets

function waveletsdf(data; mtkwargs...)
  logmtsdf = log.(multitaper(data; mtkwargs...)) # log multitaper estimator
  out      = denoise(logmtsdf, TI=true) # translation invariant denoising
  @simd for j in eachindex(out)
    @inbounds out[j] = exp(out[j]) # undo the log
  end
  out
end

# simulate some data:
const pts = range(-8.0, 8.0, length=2048)
fn(x,y)   = exp(-abs(x-y))
const Kf  = cholesky!([fn(x,y) for x in pts, y in pts])
sim = Kf.L*randn(length(pts))

# Compare some spectra:
using Plots
fgd = range(-(1/2*(pts[2]-pts[1])), (1/2*(pts[2]-pts[1])), length=2049)[1:2048]
raw_pg = fftshift(periodogram(sim, onesided=false, 
                              nfft=length(sim), window=nothing).power)
raw_mt = multitaper(sim, nw=4.0, k=6, shift=true, center=true, aweight=true)
raw_sn = multitaper(sim, k=6, shift=true, center=true, tapers=:sine)
wav_mt = waveletsdf(sim, k=6, shift=true, center=true, tapers=:sine)
plot(fgd, raw_pg, label="raw periodogram", color=:grey, linewidth=0.05,
    ylabel="power", xlabel="1/2(Δt)", ylim=(1e-5, 1000.0))
plot!(fgd, raw_mt, yaxis=:log10, xlim=(0.0, fgd[end]), 
     label="raw dpss multitaper", color=:green, linewidth=0.5)
plot!(fgd, raw_sn, label="raw sine multitaper", color=:blue)
plot!(fgd, wav_mt, label="wavelet-smoothed sine multitaper", color=:red)

