module SMultitaper

  using Statistics, LinearAlgebra, FFTW

  export multitaper, spectrogram
  
  # Internal settings for caching.
  mutable struct Settings
    TAPER_STORE_MAX::Int64
    EGVAL_STORE_MAX::Int64
    WORK_STORE_MAX::Int64
    VERBOSE::Bool
  end

  # Default kwargs.
  const d_args = (pad=0, shift=false, center=true, aweight=false, nw=4.0,
                  k=7, tapers=:dpss, eigvals=nothing, aweight_tol=0.05,
                  aweight_maxit=15, coherence=false)

  # Default storage settings.
  const SETTINGS = Settings(5, 50, 30, true)

  # Dict for storing tapers.
  const DPSS_TAPERS = Dict{Tuple{Int64, Float64, Int64}, Matrix{Float64}}()
  const SINE_TAPERS = Dict{Tuple{Int64, Int64}, Matrix{Float64}}()

  # Dict for storing eigenvalues.
  const EGVALS = Dict{Tuple{Int64, Float64, Int64}, Vector{Float64}}()

  # Dict for storing FFT plans and corresponding workspaces.
  const Ftype  = Tuple{Vector{ComplexF64}, Vector{ComplexF64}, 
                       FFTW.cFFTWPlan{ComplexF64,-1,true,1}}
  const FPLANS = Dict{Int64, Ftype}()

  # Check that the provided args and data sizes are compatible.
  function check_assert(d1, d2, args)
    @assert length(d1) == length(d2) "data lengths don't agree."
    if args[:aweight]
      @assert args[:k] > 1 "No adaptive weighting for one taper."
      @assert args[:tapers] != :sine "Adaptive sine not implemented."
      if args[:coherence]
        @warn "Coherence with adaptive weighting will not necessarily be in [0,1]."
      end
    end
    nothing
  end

  # An internal function for computing the DPSS tapers and caching the result.
  function get_dpss_eigvecs(n::Int64, nw::Float64, k::Int64)
    haskey(DPSS_TAPERS, (n, nw, k)) && return DPSS_TAPERS[(n, nw, k)]
    stdm = SymTridiagonal([cos(2*pi*(nw/n))*abs2(0.5*(n-1)-(j-1)) for j in 1:n],
                          [0.5*j*(n-j) for j in 1:(n-1)])
    vecs = eigvecs(stdm, eigvals(stdm, (n-k+1):n))
    length(DPSS_TAPERS) <  SETTINGS.TAPER_STORE_MAX && (DPSS_TAPERS[(n, nw, k)] = vecs)
    if length(DPSS_TAPERS) == SETTINGS.TAPER_STORE_MAX && SETTINGS.VERBOSE
      @warn "DPSS_TAPER cache full, no longer caching DPSS vectors."
    end
    vecs
  end

  # An internal function for computing an FFT plan and caching the result.
  function get_plan(n::Int64)
    haskey(FPLANS, n) && return FPLANS[n]
    work1 = zeros(ComplexF64, n)
    work2 = zeros(ComplexF64, n)
    plan  = plan_fft!(work1)
    length(FPLANS) <  SETTINGS.WORK_STORE_MAX && (FPLANS[n] = (work1, work2, plan))
    if length(FPLANS) == SETTINGS.WORK_STORE_MAX && SETTINGS.VERBOSE
      @warn "WORK cache full, no longer caching FFT plans and workspaces."
    end
    (work1, work2, plan)
  end

  # A very simple convolution of a thing with itself.
  function convxx(x) 
    len = nextprod([2,3,5,7], 2*length(x)-1)
    (work1, work2, plan) = get_plan(len)
    fill!(work1, zero(ComplexF64))
    work1[1:length(x)] .= x
    plan*work1
    work1.*=work1
    plan\work1
    isreal(x) ? real(work1) : work1
  end

  # Compute the DPSS functions and optionally the eigenvalues.
  function dpss_tapers(n::Int64, nw, k::Int64, eigenvalues::Bool=false)
    vecs = get_dpss_eigvecs(n, nw, k)
    !eigenvalues && return (nothing, vecs)
    haskey(EGVALS, (n, nw, k)) && return (EGVALS[(n, nw, k)], vecs)
    q_tau = reduce(hcat, map(x->convxx(x)[n:(2*n-1)], eachcol(vecs)))
    sincv = map(j->sin(2.0*pi*(nw/n)*j)/(pi*j), 1:(n-1))
    vals  = map(j->2.0*((nw/n) + dot(q_tau[2:n, j], sincv)), 1:k).*sign.(q_tau[1,:])
    length(EGVALS) <  SETTINGS.EGVAL_STORE_MAX && (EGVALS[(n, nw, k)] = vals)
    if length(EGVALS) == SETTINGS.EGVAL_STORE_MAX && SETTINGS.VERBOSE
      @warn "EGVALS cache full, no longer caching DPSS eigenvalues."
    end
    (vals, vecs)
  end

  # Compute sine tapers, and cache the result.
  function sine_tapers(n, m)
    haskey(SINE_TAPERS, (n,m)) && return SINE_TAPERS[(n,m)]
    tapers = [sqrt(2.0/(n+1))*sin(pi*k*j/(n+1)) for j in 1:n, k in 1:m]
    length(SINE_TAPERS) < SETTINGS.TAPER_STORE_MAX && (SINE_TAPERS[(n,m)] = tapers)
    tapers
  end

  # A simple helper function to handle tapering and padding.
  function taper!(V, slep, work) 
    @inbounds @simd for j in eachindex(V, slep)
      work[j] = V[j]*slep[j]
    end
    fill!(view(work, (length(V)+1):length(work)), zero(ComplexF64))
    nothing
  end

  # A simple helper function to handle cross-spectra. This could be optimized
  # further for d1 == d2.
  function fftprd(d1, d2, s, pd) 
    (work1, work2, plan) = get_plan(max(length(d1), pd))
    if d1 != d2
      for (dj, wj) in ((d1, work1), (d2, work2))
        taper!(dj, s, wj)
        plan*wj
      end
      work1.*=conj(work2)
    else
      taper!(d1, s, work1)
      plan*work1
      work1.*=conj(work1)
    end
    work1
  end

  # Get all eigenspectra, for adapative weighting, t-testing, etc.
  function especs!(d1,d2,tp,pd,out) 
    for j in 1:size(out,2)
      wrk = fftprd(d1, d2, view(tp,:,j), pd)
      map!(abs, view(out,:,j), wrk)
    end
  end

  # A dot function that is faster for very small vectors. For aweight function.
  function _dot(v1, v2)
    s = 0.0
    @inbounds @simd for j in eachindex(v1, v2)
      s += v1[j]*v2[j]
    end
    s
  end

  # A specific weight updating function. For aweight function.
  function weightupdate!(new, est, evalues, evar)
    @inbounds @simd for j in eachindex(new, evalues)
      new[j] = sqrt(evalues[j])*est/(evalues[j]*est+evar*(1.0-evalues[j]))^2
    end
    nothing
  end

  # Obtained adaptively weighted estimates. Somewhat micro-optimized.
  function aweighted(old, new, len, estimates, evalues, evar, maxit, tol)
    est = _dot(estimates, old)/sum(old)
    for _ in 1:maxit
      weightupdate!(new, est, evalues, evar)
      new_est = _dot(estimates, new)/sum(new)
      isapprox(est, new_est, rtol=tol) && return new_est
      est  = new_est
      old .= new
    end
    est
  end

  # Get tapers from kwargs and data length.
  function get_tapers(args, len)
    if isnothing(args[:eigvals]) && args[:tapers] == :dpss
      return dpss_tapers(len, args[:nw], args[:k], args[:aweight])
    elseif isnothing(args[:eigvals]) && args[:tapers] == :sine
      return (nothing, sine_tapers(len, args[:k]))
    elseif isnothing(args[:eigvals]) && args[:tapers] isa Matrix
      return (nothing, args[:tapers])
    elseif args[:eigvals] isa Vector && args[:tapers] isa Matrix
      return (args[:eigvals], args[:tapers])
    else
      throw(error("Invalid taper specification."))
    end
  end

  # In-place multitaper that updates the out vector. kwargs not checked.
  function multitaper!(out, cwork, cohwork, eigspecs, d1, d2=d1; kwargs...)
    (_d1, _d2) = kwargs[:center] ? (d1.-mean(d1), d2.-mean(d2)) : (d1, d2)
    (vals, tapers) = get_tapers(kwargs, length(d1))
    if kwargs[:aweight]
      especs!(_d1, _d2, tapers, kwargs[:pad], eigspecs)
      proc_var = abs(sum(prod, zip(_d1, conj(_d2)))/length(_d1))
      (k, maxit, tol) = kwargs[:k], kwargs[:aweight_maxit], kwargs[:aweight_tol]
      (old, new) = vcat(zeros(k-2), ones(2).*0.5), zeros(k) 
      for j in eachindex(out)
        @inbounds out[j] = aweighted(old, new, k, view(eigspecs,j,:), 
                                     vals, proc_var, maxit, tol)
      end
    else
      fill!(cwork, zero(ComplexF64))
      fill!(out, zero(Float64))
      for colj in eachcol(tapers)
        tmp      = fftprd(_d1, _d2, colj, kwargs[:pad]) # re-using a cached space.
        out    .+= abs.(tmp) # could vectorize this?
        cwork  .+= tmp       # could vectorize this?
      end
      out   ./= size(tapers, 2) # could vectorize this?
      cwork ./= size(tapers, 2) # could vectorize this?
      if kwargs[:coherence]
        coh_d = Dict(:coherence=>false, :shift=>false, :center=>false)
        cargs = merge(Dict(kwargs), coh_d)
        for dj in (_d1, _d2)
          multitaper!(out, cohwork, eigspecs, dj, dj; cargs...)
          cwork ./= sqrt.(out) # could vectorize this?
        end
      end
    end
    nothing
  end

  # Compute a multitaper estimator. Reasonably optimized for repeated calls.
  function multitaper(d1, d2=d1; kwargs...)
    args = merge(d_args, kwargs)
    check_assert(d1, d2, args)
    out   = zeros(Float64, max(length(d1), args[:pad]))
    cwrk  = zeros(ComplexF64, max(length(d1), args[:pad]))
    specs = args[:aweight] ? zeros(length(out), args[:k]) : zeros(0,0)
    cohw  = args[:coherence] ? similar(cwrk) : ComplexF64[]
    multitaper!(out, cwrk, cohw, specs, d1, d2; args...)
    if args[:coherence]
      args[:shift] ? fftshift(cwrk) : cwrk
    else
      args[:shift] ? fftshift(out) : out
    end
  end

  # A simple spectrogram. Not particularly optimized.
  function spectrogram(d1, d2=d1; blocklen, stepsize=blocklen, kwargs...)
    args = merge(d_args, kwargs)
    @assert !(args[:coherence]) "Coherence spectrogram not currently implemented."
    check_assert(d1, d2, args)
    if !(args[:tapers] isa Matrix)
      (vals, vecs) = get_tapers(args, blocklen)
      args = merge(args, (eigvals=vals, tapers=vecs))
    end
    col_len = max(blocklen, args[:pad])
    cwrk    = zeros(ComplexF64, col_len)
    chwk    = args[:coherence] ? similar(cwrk) : ComplexF64[]
    specs   = args[:aweight] ? zeros(col_len, args[:k]) : zeros(0,0)
    strt_ix = 1:stepsize:length(d1)
    stop_ix = blocklen:stepsize:length(d1)
    out     = zeros(max(blocklen, args[:pad]), length(stop_ix))
    @views for (j, (sr_ix, sp_ix)) in enumerate(zip(strt_ix, stop_ix))
      multitaper!(out[:,j], cwrk, chwk, specs, d1[sr_ix:sp_ix], 
                  d2[sr_ix:sp_ix]; args...)
    end
    args[:shift] ? reduce(hcat, map(fftshift, eachcol(out)))  : out
  end

end
